/**
 * Copyright (c) 2022 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import router from '@system.router';
import {describe, beforeAll, beforeEach, afterEach, afterAll, it, expect} from "hypium/index"
import Utils from './Utils.ets'

export default function stackAlignContentJsunit() {
  describe('stackAlignContentTest', function () {
    beforeAll(async function (done) {
      console.info("flex beforeEach start");
      let options = {
        uri: 'pages/stack',
      }
      try {
        router.clear();
        let pages = router.getState();
        console.info("get stack state success " + JSON.stringify(pages));
        if (!("stack" == pages.name)) {
          console.info("get stack state success " + JSON.stringify(pages.name));
          let result = await router.push(options);
          await Utils.sleep(2000);
          console.info("push stack page success " + JSON.stringify(result));
        }
      } catch (err) {
        console.error("push stack page error: " + err);
      }
      done()
    });

    afterEach(async function () {
      await Utils.sleep(1000);
      console.info("stackAlignContent after each called");
    });

    /*
     * @tc.number       SUB_ACE_BASIC_ETS_API_0001
     * @tc.name         teststackAlignContent0001
     * @tc.desic         acestackAlignContentEtsTest0001
     */
    it('teststackAlignContent0001', 0, async function (done) {
      console.info('stackAlignContent teststackAlignContent0001 START');
      await Utils.sleep(2000);
      let strJson = getInspectorByKey('Stack');
      console.info("[teststackAlignContent0001] component width strJson:" + strJson);
      let obj = JSON.parse(strJson);
      expect(obj.$type).assertEqual('Stack');
      expect(obj.$attrs.width).assertEqual("100.00%");
      console.info("[teststackAlignContent0001] width value :" + obj.$attrs.width);
      done();
    });

    /*
     * @tc.number       SUB_ACE_BASIC_ETS_API_0002
     * @tc.name         teststackAlignContent0002
     * @tc.desic         acestackAlignContentEtsTest0002
     */
    it('teststackAlignContent0002', 0, async function (done) {
      console.info('stackAlignContent teststackAlignContent0002 START');
      await Utils.sleep(2000);
      let strJson = getInspectorByKey('Stack');
      console.info("[teststackAlignContent0002] component width strJson:" + strJson);
      let obj = JSON.parse(strJson);
      expect(obj.$type).assertEqual('Stack');
      expect(obj.$attrs.height).assertEqual("150.00vp");
      console.info("[teststackAlignContent0002] width value :" + obj.$attrs.width);
      done();
    });
  })
}
